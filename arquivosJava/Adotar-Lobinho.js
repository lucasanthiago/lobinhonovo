

var LoboFoto = document.querySelector("#Lobo-Foto");
var LoboNome = document.querySelector("#Lobo-Nome");
var LoboID = document.querySelector("#Lobo-ID");


var NomeAdota = document.querySelector("#Adota-Nome").value;
var IdadeAdota = document.querySelector("#Adota-Idade").value;
var EmailAdota = document.querySelector("#Adota-Email").value;

var SendAdotarLobo = document.querySelector("#Send-Adotar-Lobo");


LobosPromise.then(Lobos => {
    
    const queryURL = window.location.search;

    const urlParams = new URLSearchParams(queryURL);

    let ID = urlParams.get('id');

    var LoboObj = [];

    for (let i=0;i<Lobos.length;i++){
       if (Lobos[i].id == ID){
        LoboObj.push(Lobos[i]);
       }
    }

    LoboFoto.src = `${LoboObj[0].image_url}`;
    LoboNome.innerText = `Adote o(a) ${LoboObj[0].name}`;
    LoboID.innerText = `id: ${ID}`;
    
    
    

});




SendAdotarLobo.addEventListener("click", (e) => {

    const queryURL = window.location.search;

    const urlParams = new URLSearchParams(queryURL);

    let ID = urlParams.get('id');


    /* INPUT VALIDATION ######################################   */

    let NomeValidate = false;
    let AnosValidate = false;
    let EmailValidate = false;


    var hasNumber = /\d/;

    var hasNAN = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;


    if (NomeAdota.length < 1){
        NomeValidate = false;
    }else{
        if (hasNumber.test(NomeAdota) == true || hasNAN.test(NomeAdota) == true){
            NomeValidate = false;
        }else{
            NomeValidate = true;
        }
    }

    if (IdadeAdota.length == 0){
        AnosValidate = false;
    }else{
        AnosValidate = true;
    }

    var regexCOM = /\S+@\S+\.\S+/;
    var regexCOMBR = /\S+@\S+\.\S+.\S+/;

    if (regexCOM.test(EmailAdota) || regexCOMBR.teste(EmailAdota)){
        EmailValidate = true;
    }else{
        EmailValidate = false;
    }

    
    if (NomeValidate && AnosValidate && EmailValidate){

        let wolf = {
            wolf:{
                adopter_name: NomeAdota,
                adopter_age: IdadeAdota,
                adopter_email: EmailAdota
            }     
        }
    
        let FetchConfig = {
            method: 'PUT',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(wolf)
        }
    
        fetch(`https://lobinhos.herokuapp.com/wolves/${ID}`,FetchConfig).then(response => response.json()).then(data => {
            console.log('Success:',data);
        }).catch(error => {
            console.log('Error',error);
        });
      
    }



});

