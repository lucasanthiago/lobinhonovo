const url = "https://lobinhos.herokuapp.com/wolves";
const url_adopted = "https://lobinhos.herokuapp.com/wolves/adopted";

let FetchConfig = {
    method: "GET"
}


const LobosPromise = fetch(url, FetchConfig).then(response => {
    return response.json();
});


const LobosAdoptedPromise = fetch(url_adopted, FetchConfig).then(response => {
    return response.json();
});