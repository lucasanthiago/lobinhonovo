const url = "https://lobinhos.herokuapp.com/wolves";

var FormSubmitButton = document.querySelector("#Form-Submit");


FormSubmitButton.addEventListener("click", (e) => {

    e.preventDefault();
    let NomeLobinho = document.getElementById("nome_lobinho").value;
    let AnosLobinho = document.getElementById("anos_lobinho").value;
    let LinkLobinho = document.getElementById("link_lobinho").value;
    let DescLobinho = document.getElementById("desc_lobinho").value;
    
    /* INPUT VALIDATION ######################################   */

    var hasNumber = /\d/;

    var hasNAN = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    


    let NomeValidate = false;
    let AnosValidate = false;
    let LinkValidate = false;
    let DescValidate = false;


    if (NomeLobinho.length < 4 || NomeLobinho.length > 60){
        NomeValidate = false;
    }else{
        if (hasNumber.test(NomeLobinho) == true || hasNAN.test(NomeLobinho) == true){
            NomeValidate = false;
        }else{
            NomeValidate = true;
        }
    }

    if (AnosLobinho.length == 0){
        AnosValidate = false;
    }else{
        if (parseInt(AnosLobinho) < 0 || parseInt(AnosLobinho) > 100){
            AnosValidate = false;
        }else{
            AnosValidate = true;
        }
    }

    if (LinkLobinho.length == 0){
        LinkValidate = false;
    }else{
        LinkValidate = true;
    }

    if (DescLobinho.length < 10 || DescLobinho.length > 255){
        DescValidate = false;
    }else{
        DescValidate = true;
    }
    
    

    if (NomeValidate && AnosValidate && LinkValidate && DescValidate){

        let wolf = {
            wolf:{
                name: NomeLobinho,       
                age: AnosLobinho,       
                image_url: LinkLobinho,       
                description: DescLobinho
            }     
        }
    
        let FetchConfig = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(wolf)
        }

        fetch(url,FetchConfig).then(response => response.json()).then(data => {
            console.log('Success:',data);
        }).catch(error => {
            console.log('Error',error);
        });
      
    }else{
        alert("Preencha corretamente o formulário");
    }
    
    


  


    
});