
var Page = 1;

var PreviousPageButton = document.querySelector("#Previous-Page");
var NextPageButton = document.querySelector("#Next-Page");

var Adotar1 = document.querySelector("#Adotar1");
var Adotar2 = document.querySelector("#Adotar2");
var Adotar3 = document.querySelector("#Adotar3");
var Adotar4 = document.querySelector("#Adotar4");


var Foto1 = document.querySelector("#Foto1");
var Name1 = document.querySelector("#Name1");
var Idade1 = document.querySelector("#Idade1");
var Texto1 = document.querySelector("#Texto1");
var ID1 = document.querySelector("#Hidden-ID1");
var AdoptedBy1 = document.querySelector("#Adopted-By1");


var Foto2 = document.querySelector("#Foto2");
var Name2 = document.querySelector("#Name2");
var Idade2 = document.querySelector("#Idade2");
var Texto2 = document.querySelector("#Texto2");
var ID2 = document.querySelector("#Hidden-ID2");
var AdoptedBy2 = document.querySelector("#Adopted-By2");

var Foto3 = document.querySelector("#Foto3");
var Name3 = document.querySelector("#Name3");
var Idade3 = document.querySelector("#Idade3");
var Texto3 = document.querySelector("#Texto3");
var ID3 = document.querySelector("#Hidden-ID3");
var AdoptedBy3 = document.querySelector("#Adopted-By3");

var Foto4 = document.querySelector("#Foto4");
var Name4 = document.querySelector("#Name4");
var Idade4= document.querySelector("#Idade4");
var Texto4 = document.querySelector("#Texto4");
var ID4 = document.querySelector("#Hidden-ID4");
var AdoptedBy4 = document.querySelector("#Adopted-By4");


var AdoptedCheckBox = document.querySelector("#Adopted-CheckBox");







var Offset = 0;



function GetLobos(status){


    let LobosParaAdotar = [];

    let LobosAdotados = [];

    if (status == 'Adotados'){
        LobosAdoptedPromise.then(LobosAdopted => {

            let NewArrAdopted = [];

            for (let c=Offset;c<Offset+4;c++){
                NewArrAdopted.push(LobosAdopted[c]);
            }

            Foto1.style.backgroundImage = `url('${NewArrAdopted[0].image_url}')`;
            Foto2.style.backgroundImage = `url('${NewArrAdopted[1].image_url}')`;
            Foto3.style.backgroundImage = `url('${NewArrAdopted[2].image_url}')`;
            Foto4.style.backgroundImage = `url('${NewArrAdopted[3].image_url}')`;
            Name1.innerText = `${NewArrAdopted[0].name}`;
            Name2.innerText = `${NewArrAdopted[1].name}`;
            Name3.innerText = `${NewArrAdopted[2].name}`;
            Name4.innerText = `${NewArrAdopted[3].name}`;
            Idade1.innerText = `Idade: ${NewArrAdopted[0].age} anos`;
            Idade2.innerText = `Idade: ${NewArrAdopted[1].age} anos`;
            Idade3.innerText = `Idade: ${NewArrAdopted[2].age} anos`;
            Idade4.innerText = `Idade: ${NewArrAdopted[3].age} anos`;
            Texto1.innerText = NewArrAdopted[0].description;
            Texto2.innerText = NewArrAdopted[1].description;
            Texto3.innerText = NewArrAdopted[2].description;
            Texto4.innerText = NewArrAdopted[3].description;
            ID1.innerText = NewArrAdopted[0].id;
            ID2.innerText = NewArrAdopted[1].id;
            ID3.innerText = NewArrAdopted[2].id;
            ID4.innerText = NewArrAdopted[3].id;
            AdoptedBy1.innerText = `Adotado por: ${NewArrAdopted[0].adopter_name}`;
            AdoptedBy1.style.display = 'Flex';
            AdoptedBy2.innerText = `Adotado por: ${NewArrAdopted[1].adopter_name}`;
            AdoptedBy2.style.display = 'Flex';
            AdoptedBy3.innerText = `Adotado por: ${NewArrAdopted[2].adopter_name}`;
            AdoptedBy3.style.display = 'Flex';
            AdoptedBy4.innerText = `Adotado por: ${NewArrAdopted[3].adopter_name}`;
            AdoptedBy4.style.display = 'Flex';
            Adotar1.style.background = '#7AAC3A';
            Adotar1.innerText = 'Adotado';
            Adotar2.style.background = '#7AAC3A';
            Adotar2.innerText = 'Adotado';
            Adotar3.style.background = '#7AAC3A';
            Adotar3.innerText = 'Adotado';
            Adotar4.style.background = '#7AAC3A';
            Adotar4.innerText = 'Adotado';

            Offset += 4;
            Page += 1;
        });
    }else if (status == 'ParaAdocao'){
        LobosPromise.then(Lobos => {
    
            let NewArr = [];

            for (let j=Offset;j<Offset+4;j++){
                NewArr.push(Lobos[j]);
            }
            
            Foto1.style.backgroundImage = `url('${NewArr[0].image_url}')`;
            Foto2.style.backgroundImage = `url('${NewArr[1].image_url}')`;
            Foto3.style.backgroundImage = `url('${NewArr[2].image_url}')`;
            Foto4.style.backgroundImage = `url('${NewArr[3].image_url}')`;
            Name1.innerText = `${NewArr[0].name}`;
            Name2.innerText = `${NewArr[1].name}`;
            Name3.innerText = `${NewArr[2].name}`;
            Name4.innerText = `${NewArr[3].name}`;
            Idade1.innerText = `Idade: ${NewArr[0].age} anos`;
            Idade2.innerText = `Idade: ${NewArr[1].age} anos`;
            Idade3.innerText = `Idade: ${NewArr[2].age} anos`;
            Idade4.innerText = `Idade: ${NewArr[3].age} anos`;
            Texto1.innerText = NewArr[0].description;
            Texto2.innerText = NewArr[1].description;
            Texto3.innerText = NewArr[2].description;
            Texto4.innerText = NewArr[3].description;
            ID1.innerText = NewArr[0].id;
            ID2.innerText = NewArr[1].id;
            ID3.innerText = NewArr[2].id;
            ID4.innerText = NewArr[3].id;
    
            Offset += 4;
            Page += 1;
        });
    }
    
    
}


if (Offset == 0){
    if (AdoptedCheckBox.checked){
        GetLobos('Adotados');
    }else{
        GetLobos('ParaAdocao');
    }
}



NextPageButton.addEventListener("click", () =>{

    if (AdoptedCheckBox.checked){
        GetLobos('Adotados');
    }else{
        GetLobos('ParaAdocao');
    }

});


Adotar1.addEventListener("click",(e) =>{

    var ID = e.currentTarget.parentElement.nextElementSibling.innerText;
    window.location.replace(`Show-Lobinho.html?id=${ID}`);

});

Adotar2.addEventListener("click", (e) =>{

    var ID = e.currentTarget.parentElement.nextElementSibling.innerText;
    window.location.replace(`Show-Lobinho.html?id=${ID}`);

});

Adotar3.addEventListener("click", (e) =>{

    var ID = e.currentTarget.parentElement.nextElementSibling.innerText;
    window.location.replace(`Show-Lobinho.html?id=${ID}`);

});

Adotar4.addEventListener("click", (e) =>{

    var ID = e.currentTarget.parentElement.nextElementSibling.innerText;
    window.location.replace(`Show-Lobinho.html?id=${ID}`);

});


AdoptedCheckBox.addEventListener("change", (e) => {

    Page = 0;
    Offset = 0;

    if (e.currentTarget.checked){
        GetLobos('Adotados');
    }else if (!e.currentTarget.checked){
        GetLobos('ParaAdocao');
    }

});