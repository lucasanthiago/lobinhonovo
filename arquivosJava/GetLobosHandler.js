

function GetNLobosID(N,PromiseObj){
    let min = 0;
    let max = PromiseObj.length;

    let RandomIndex = [];
    for (let i=0;i<N;i++){
        let random = Math.floor(Math.random()*max+1);
        RandomIndex.push(random);
    }

    return RandomIndex;
}

var Lobo1 = document.querySelector(".Lobo-Box:nth-child(even) .Lobo-Foto");

var Lobo2 = document.querySelector(".Lobo-Box:nth-child(odd) .Lobo-Foto");

var Idade1 = document.querySelector(".Lobo-Box:nth-child(even) .Lobo-Idade");

var Idade2 = document.querySelector(".Lobo-Box:nth-child(odd) .Lobo-Idade");

var Nome1 = document.querySelector(".Lobo-Box:nth-child(even) .Lobo-Name");

var Nome2 = document.querySelector(".Lobo-Box:nth-child(odd) .Lobo-Name");

var Descricao1 = document.querySelector(".Lobo-Box:nth-child(even) .Lobo-Texto");

var Descricao2 = document.querySelector(".Lobo-Box:nth-child(odd) .Lobo-Texto");

LobosPromise.then(Lobos => {

    let Indexes = GetNLobosID(2,Lobos);

    let LobosObj = [];


    for (let j=0;j<Indexes.length;j++){
        LobosObj.push(Lobos[Indexes[j]]);
    }
   


    Lobo1.style.backgroundImage = `url('${LobosObj[0].image_url}')`;
    
    Lobo2.style.backgroundImage = `url('${LobosObj[1].image_url}')`;

    Idade1.innerText = `Idade: ${LobosObj[0].age} anos`;

    Idade2.innerText = `Idade: ${LobosObj[1].age} anos`;

    Nome1.innerText = `${LobosObj[0].name}`;

    Nome2.innerText = `${LobosObj[1].name}`;
 
    Descricao1.innerText = LobosObj[0].description;

    Descricao2.innerText = LobosObj[1].description;

});

LobosAdoptedPromise.then(LobosAdopted => {
    console.log(LobosAdopted);
});