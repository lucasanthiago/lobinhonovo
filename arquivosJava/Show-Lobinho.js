
var AdotarLoboButton = document.querySelector("#Adotar-Lobo");
var DeletarLoboButton = document.querySelector("#Excluir-Lobo");

var LoboFoto = document.querySelector("#Lobo-Foto");
var LoboNome = document.querySelector("#Lobo-Nome");
var LoboTexto = document.querySelector("#Lobo-Texto");

LobosPromise.then(Lobos => {
    
    const queryURL = window.location.search;

    const urlParams = new URLSearchParams(queryURL);

    let ID = urlParams.get('id');

    var LoboObj = [];

    for (let i=0;i<Lobos.length;i++){
       if (Lobos[i].id == ID){
        LoboObj.push(Lobos[i]);
       }
    }

    LoboFoto.src = `${LoboObj[0].image_url}`;
    LoboNome.innerText = LoboObj[0].name;
    LoboTexto.innerText = LoboObj[0].description;
    
    

});

const queryURL = window.location.search;

const urlParams = new URLSearchParams(queryURL);

let ID = urlParams.get('id');


AdotarLoboButton.addEventListener("click", (e) =>{

    window.location.replace(`Adotar-Lobinho.html?id=${ID}`);

});

DeletarLoboButton.addEventListener("click", (e) =>{

    fetch(`${url}/${ID}`,{
        method:'DELETE',
    }).then(res => res.text())
    .then(res => console.log(res))
});